Ansible role to install tor.

This role just pull and install tor from package by default.
It is used by others role to support hidden services. For example,
on https://gitlab.com/osas/ansible-role-httpd have a setting 
`use_hidden_service`.

# Deploying a relay

If you wish to deploy a relay on tor, you can do it this way:

```
- hosts: torrelay
  roles:
  - role: tor
    enable_relay: True
    # in megabytes
    relay_bandwidth: '500'   
```

## Variables and defaults values for relay

### Relay Port

To change the default port from 9001 to something else, you can pass the variable `relay_port`.
Future version of the role will also take care of the firewall, but that's not the case for now.

### Nickname

Nickname is by default set to the hostname of the relay. If you wish to change it, just use
`nickname` when declaring the role.
